from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.response import Response

from .models import Meal
from .serializers import MealSerializer


class MealViewSet(viewsets.ViewSet):

    """
    Provides ViewSet for listing or retrieving Meal objects.
    """

    model = Meal

    def list(self, request):
        queryset = Meal.objects.all()
        permission_classes = (DjangoModelPermissions,)
        serializer = MealSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Meal.objects.all()
        permission_classes = (DjangoModelPermissions,)
        user = get_object_or_404(queryset, pk=pk)
        serializer = MealSerializer(user)
        return Response(serializer.data)