from django.contrib import admin

from leaflet.admin import LeafletGeoAdmin

from .models import Meal


class MealAdmin(LeafletGeoAdmin):
  inlines = []


admin.site.register(Meal, MealAdmin)
