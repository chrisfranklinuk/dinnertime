from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.contrib.gis.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField

from users.models import User

@python_2_unicode_compatible
class Meal(models.Model):
    
    """
    Stores Meal objects
    """
    
    # General
    name = models.CharField(_('name'), help_text=_('Name of the meal'), max_length=255)
    slug = AutoSlugField(populate_from='name')
    datetime = models.DateTimeField(_('datetime'), help_text=_('When is the meal?'), blank=True, null=True)

    # User relationships
    created_by = models.ForeignKey(User, help_text=_('Who created the meal?'), related_name='created_meals')
    hosted_by = models.ManyToManyField(User, help_text=_('Who is hosting?'), related_name='hosted_meals' , blank=True, null=True)
    attended_by = models.ManyToManyField(User, help_text=_('Who is coming?'), related_name='attended_meals' , blank=True, null=True)
    declined_by = models.ManyToManyField(User, help_text=_('Who is not coming?'), related_name='declined_meals' , blank=True, null=True)
    invited = models.ManyToManyField(User, help_text=_('Who is invited?'), related_name='invited_meals' , blank=True, null=True)
    
    max_guests = models.IntegerField(_('max_guests'), help_text=_('How many guests can come?'), default=5)

    # Privacy
    hidden = models.BooleanField(_('hidden'), help_text=_('Is the meal a secret?'), default=False)
    invite_only = models.BooleanField(_('invite_only'), help_text=_('Is the meal invite only?'), default=False)

    # GeoDjango-specific:
    objects = models.GeoManager()
    location = models.PointField()

    class Meta:
        verbose_name = _('Meal')
        verbose_name_plural = _('Meals')

    def __str__(self):
        return '{}'.format(self.name or 'Unsaved')

    def get_absolute_url(self):
        return reverse('meals:meal_detail', args=[str(self.id)])

    @property
    def invited_count(self):
        return self.invited.objects.all().count()

    @property
    def attended_count(self):
        return self.attended_by.objects.all().count()

    def invite_user(self, user):
        return self.invited.objects.add(user)

    def attend(self, user):
        if self.invited.objects.contains(user):
            # The user was invited, add them to attended
            self.attended_by.objects.add(user)
            # Remove the user from invited
            self.invited.object.remove(user)

            return "Not implemented"
        else:
            #The user was not invited or has already accepted return an error.
            return "403" # TODO: Look at how this returns

    @property
    def geom(self):
        return self.location
