from django.conf.urls import include, patterns, url
from rest_framework import routers

from djgeojson.views import GeoJSONLayerView

from .views import MealList, MealCreate, MealDetail, MealUpdate, MealDelete
from .api import MealViewSet
from .models import Meal


router = routers.DefaultRouter()
router.register(r'meals', MealViewSet)

urlpatterns = patterns(
    '',
    url(
    	r'^$', 
    	MealList.as_view(), 
    	name='meal_list'
    ),
    url(
    	r'^new/$', 
    	MealCreate.as_view(), 
    	name='meal_create'
    ),
    url(
    	r'^(?P<pk>\d+)/$', 
    	MealDetail.as_view(), 
    	name='meal_detail'
    ),
    url(
    	r'^(?P<pk>\d+)/update/$', 
	    MealUpdate.as_view(), 
	    name='meal_update'
    ),
    url(
    	r'^(?P<pk>\d+)/delete/$', 
	    MealDelete.as_view(), 
	    name='meal_delete'
    ),
)


urlpatterns = urlpatterns + patterns('',
    url(r'^data.geojson$', GeoJSONLayerView.as_view(model=Meal), name='geodata')
)
