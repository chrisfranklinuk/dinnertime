from rest_framework import serializers
from .models import Meal
from users.serializers import UserSerializer

class MealSerializer(serializers.ModelSerializer):

    """
    Serializes Meal objects.
    """

    created_by = UserSerializer(partial=True, required=False, many=False)

    hosted_by = UserSerializer(partial=True, required=False, many=True)
    invited = UserSerializer(partial=True, required=False, many=True)
    attended_by = UserSerializer(partial=True, required=False, many=True)

    class Meta:
        model = Meal
        fields = (
            'id',
            'name',

            'datetime',

            'created_by',
            'hosted_by',
            'invited',
            'attended_by',

            'max_guests',
            #'invited_count',
            #'attended_count',

            'location',
        )