import floppyforms as forms

from leaflet.forms.widgets import LeafletWidget

from .models import Meal


class MealForm(forms.ModelForm):
    class Meta:
        model = Meal
        widgets = {'where': LeafletWidget()}
