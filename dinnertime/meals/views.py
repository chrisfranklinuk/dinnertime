from django.core.urlresolvers import reverse
from vanilla import ListView, CreateView, DetailView, UpdateView, DeleteView
from .forms import MealForm
from .models import Meal


class MealCRUDView(object):

    """
    Provides a mixin for Meal objects
    """

    model = Meal
    form_class = MealForm
    paginate_by = 20

    def get_success_url(self):
        return reverse('meal_list')


class MealList(MealCRUDView, ListView):

    """
    Provides a view to list Meal objects
    """

    pass


class MealCreate(MealCRUDView, CreateView):

    """
    Provides a view to create Meal objects
    """

    pass


class MealDetail(MealCRUDView, DetailView):

    """
    Provides a view to show a specific Meal object
    """

    pass


class MealUpdate(MealCRUDView, UpdateView):

    """
    Provides a view to update a specific Meal object
    """

    pass


class MealDelete(MealCRUDView, DeleteView):

    """
    Provides a view to delete a specific Meal object
    """

    pass
