# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from rest_framework import routers
from meals.api import MealViewSet
from users.api import UserViewSet

urlpatterns = patterns('',
    url(r'^$',
        TemplateView.as_view(template_name='pages/home.html'),
        name="home"),
    url(r'^about/$',
        TemplateView.as_view(template_name='pages/about.html'),
        name="about"),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # User management
    url(r'^users/', include("users.urls", namespace="users")),
    url(r'^accounts/', include('allauth.urls')),

    # Uncomment the next line to enable avatars
    url(r'^avatar/', include('avatar.urls')),

    # Your stuff: custom urls go here
    url(r'^meals/', include("meals.urls", namespace="meals")),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# API related url patterns
router = routers.DefaultRouter()
router.register(r'meals', MealViewSet)
router.register(r'users', UserViewSet)

urlpatterns = urlpatterns + patterns (
    '',
    url(
        r'^api/', 
        include(router.urls)
    ),
)