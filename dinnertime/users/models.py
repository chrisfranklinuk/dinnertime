# -*- coding: utf-8 -*-
import hashlib

try:
    from urllib.parse import urljoin, urlencode
except ImportError:
    from urlparse import urljoin
    from urllib import urlencode

# Import the AbstractUser model
from django.contrib.auth.models import AbstractUser

# Import the basic Django ORM models library
from django.db import models

from django.utils.translation import ugettext_lazy as _

from multiselectfield import MultiSelectField

from avatar.conf import settings
from avatar.util import get_primary_avatar, get_default_avatar_url, force_bytes
from avatar.models import Avatar

# Subclass AbstractUser
class User(AbstractUser):

    DIET_CHOICES = (
        ("MEAT", 'Meat Eater'),
        ("VEGETARIAN", 'Vegetarian'),
        ("VEGAN", 'Vegan'),
        ("PESKY", 'Pescatarian'),
        ("HALAL", "Halal"),
        ("KOSHA", "Kosha"),
    )
    diet = models.CharField(max_length=10, choices=DIET_CHOICES, default="MEAT")
    ALLERGY_CHOICES = (
        ("MILK", 'Milk'),
        ("EGGS", 'Eggs'),
        ("FISH", 'Fish'),
        ("SHELLFISH", 'Crustacean/Shellfish'),
        ("TREENUTS", 'Tree Nuts'),
        ("PEANUTS", 'Peanuts'),
        ("WHEAT", 'Wheat'),
        ("SOYBEANS", 'Soybeans'),
    )
    allergies = MultiSelectField(max_length=20, blank=True, choices=ALLERGY_CHOICES)
    avatar_url = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.username

    def save(self, *args, **kwargs):
        # store avatar url 
        self.avatar_url = self.get_avatar_url()
        print self.avatar_url
        super(User, self).save(*args, **kwargs) # Call the "real" save() method.

    def get_avatar_url(self, size=settings.AVATAR_DEFAULT_SIZE):
        avatar = get_primary_avatar(self, size=size)
        if avatar:
            return avatar.avatar_url(size)

        if settings.AVATAR_GRAVATAR_BACKUP:
            params = {'s': str(size)}
            if settings.AVATAR_GRAVATAR_DEFAULT:
                params['d'] = settings.AVATAR_GRAVATAR_DEFAULT
            path = "%s/?%s" % (hashlib.md5(force_bytes(self.email)).hexdigest(),
                               urlencode(params))
            return urljoin(settings.AVATAR_GRAVATAR_BASE_URL, path)

        return get_default_avatar_url()

#TODO: Add signal for avatar model changes and update avatar_url