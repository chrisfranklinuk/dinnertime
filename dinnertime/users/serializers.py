from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):

    """
    Serializes User objects.
    """

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'avatar_url',
            'first_name',
            'last_name',
            'groups'
        )