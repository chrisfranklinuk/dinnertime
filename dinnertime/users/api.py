from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.response import Response
from .models import User
from .serializers import UserSerializer


class UserViewSet(viewsets.ViewSet):

    """
    Provides ViewSet for listing or retrieving User objects.
    """
    model = User

    def list(self, request):
        queryset = User.objects.all()
        permission_classes = (DjangoModelPermissions,)
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        permission_classes = (DjangoModelPermissions,)
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)