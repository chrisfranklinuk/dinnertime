# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from avatar.models import Avatar

from .models import User


class AvatarInline(admin.TabularInline):
    model = Avatar
    extra = 1


class UserAdmin(admin.ModelAdmin):
    create_form_class = UserCreationForm
    update_form_class = UserChangeForm
    inlines = [AvatarInline,]


admin.site.register(User, UserAdmin)