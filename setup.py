# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import dinnertime
version = dinnertime.__version__

setup(
    name='dinnertime',
    version=version,
    author='',
    author_email='chris@piemonster.me',
    packages=[
        'dinnertime',
    ],
    include_package_data=True,
    install_requires=[
        'Django>=1.6.1',
    ],
    zip_safe=False,
    scripts=['dinnertime/manage.py'],
)