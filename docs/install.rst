Install
=========

This is where you write how to get a new laptop to run this project.

Configure Virtualenv
--------------------

virtualenv env
source env/bin/activate

Install Project Requirements
----------------------------

pip install -r requirements.txt


Install GeoDjango
-----------------

Follow the instructions at https://docs.djangoproject.com/en/dev/ref/contrib/gis/install